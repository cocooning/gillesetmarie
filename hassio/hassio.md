---
title: hassio
description: 
published: true
date: 2022-09-16T06:03:41.796Z
tags: 
editor: markdown
dateCreated: 2022-09-07T05:05:15.348Z
---
# hassio
> Créer un ou des volumes NFS pour le montage __media__
## Pré-requis
- repertoire __media__ monté
```bash
/.cocooning/data/media
```
- volmune NFS monté
```bash
/.cocooning/<ss-domaine>/hassio
```
- Database __homeassistant__ crée et running sous mariadb, postgres ou sqlite

## Les variables d'environnement
```bash
${ENV_SYSLOG_IP}
${ENV_HASSIO_NODE_DEPLOY}
${ENV_TZ}
${ENV_HASSIO_DATABASE_CONNEXION} # Voir ci-dessous
```

```bash
postgresql://<login>:<password>@<ip>/homeassistant
mysql://<login>:<password>@mariadb/homeassistant?unix_socket=/.cocooning/data/mysqld.sock&charset=utf8mb4
mysql://<login>:<password>@mariadb:3306/homeassistant?charset=utf8mb4
sqlite:////db/home-assistant_v2.db 
```
## Configuration
se trouve dans le répertoire __/.cocooning/<ss-domaine>/hassio__
## Installation

```bash
docker stack deploy --compose-file /.cocooning/docker-stack/hassio/hassio-docker-compose.yml hassio
```
## Post-installation 

Travailler le fichier __configuration.yaml__